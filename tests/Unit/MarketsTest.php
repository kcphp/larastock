<?php

namespace Tests\Unit;

use App\Market;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MarketsTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;


    public function testCreateMarket() {
        Market::create(['name' => 'name', 'acronym' => 'NAM', 'description' => 'description']);

        $markets = Market::getAllMarkets();
        $this->assertCount(1, $markets);
    }

    public function testGetActiveMarkets() {
        Market::create(['name' => 'name', 'acronym' => 'NAM', 'description' => 'description']);
        Market::create(['name' => 'inactive', 'acronym' => 'NAM2', 'description' => 'description inactive', 'active' => 0]);

        $markets = Market::getAllMarkets();
        $this->assertCount(2, $markets);

        $active_markets = Market::getActiveMarkets();
        $this->assertCount(1, $active_markets);
    }

    public function testMarketsUsingFactories() {

        $active_markets_amount = 5;
        factory(Market::class, $active_markets_amount)->create();
        factory(Market::class)->create(['active' => 0]);

        $active_markets = Market::getActiveMarkets();
        $this->assertCount($active_markets_amount, $active_markets);
    }

    public function testFailsValidation() {
        $input = [];
        $market = new Market($input);

        $valid = $market->validate($input);
        $this->assertFalse($valid);
        $this->assertArrayHasKey('name', $market->errors->getMessages());
        $this->assertArrayHasKey('acronym', $market->errors->getMessages());
        $this->assertArrayHasKey('description', $market->errors->getMessages());
    }

    public function testPassesValidation() {
        $input = ['name' => 'name', 'acronym' => 'NAM', 'description' => 'description'];
        $market = new Market($input);

        $valid = $market->validate($input);
        $this->assertTrue($valid);

        $market->save();
        $active_markets = Market::getActiveMarkets();
        $this->assertCount(1, $active_markets);

    }
}
