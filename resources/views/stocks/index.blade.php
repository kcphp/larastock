@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$title or 'Stocks'}}</div>
                    <div class="panel-body">
                        @if(Session::has('status_message'))
                            <p class="alert alert-success">{{ Session::get('status_message') }}</p>
                        @endif
                        @if(Session::has('error_message'))
                            <p class="alert alert-danger">{{ Session::get('error_message') }}</p>
                        @endif

                        @if (!Auth::check())
                            <p class="alert alert-info">To be able to susbcribe to a stock, please <a href="/login">login</a></p>
                        @endif

                        <ul class="list-group">
                            @foreach($stocks as $stock)

                                <li class="list-group-item row">
                                    <div class="col-lg-6 col-xs-6">{{$stock->id}} - {{$stock->name}}
                                        - {{$stock->acronym}}</div>

                                    <div class="col-lg-6 col-xs-6">
                                        <div class="pull-right">

                                            <div class="btn-group">
                                                @if (Auth::check())
                                                    @if (in_array($stock->id, $user_stocks))
                                                        <form action="/user_stocks" method="POST">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <input type="hidden" name="stock_id" value="{{$stock->id}}">
                                                            <button type="submit" class="btn btn-danger">
                                                                Unsubscribe
                                                            </button>
                                                        </form>
                                                    @else
                                                        <form action="/user_stocks" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="stock_id" value="{{$stock->id}}">
                                                            <button type="submit" class="btn btn-primary">
                                                                Subscribe
                                                            </button>
                                                        </form>
                                                    @endif
                                                @endif
                                            </div>

                                            <div class="btn-group">
                                                <a class="btn btn-info" href="/stock_historical/{{$stock->id}}">View</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
