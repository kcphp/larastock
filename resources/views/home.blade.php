@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h2>Stocks a los que estás suscrito</h2>

                        @foreach($user_stocks as $user_stock)

                            <li class="list-group-item row">
                                <div class="col-lg-6 col-xs-6">{{$user_stock->stock->id}} - {{$user_stock->stock->name}}
                                    - {{$user_stock->stock->acronym}}</div>

                                <div class="col-lg-6 col-xs-6">
                                    <div class="pull-right">

                                        <div class="btn-group">
                                            <form action="/user_stocks" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <input type="hidden" name="stock_id" value="{{$user_stock->stock->id}}">
                                                <button type="submit" class="btn btn-danger">
                                                    Unsubscribe
                                                </button>
                                            </form>

                                        </div>

                                        <div class="btn-group">
                                            <a class="btn btn-info" href="/stock_historical/{{$user_stock->stock->id}}">View</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
