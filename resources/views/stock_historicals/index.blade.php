@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-6 col-xs-6">{{$title or 'Stock Historical'}}</div>

                            <div class="col-lg-6 col-xs-6">
                                <div class="pull-right">

                                    <div class="btn-group">
                                        @if (Auth::check())
                                            @if (in_array($stock_data->id, $user_stocks))
                                                <form action="/user_stocks" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <input type="hidden" name="stock_id" value="{{$stock_data->id}}">
                                                    <button type="submit" class="btn btn-danger">
                                                        Unsubscribe
                                                    </button>
                                                </form>
                                            @else
                                                <form action="/user_stocks" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="stock_id" value="{{$stock_data->id}}">
                                                    <button type="submit" class="btn btn-primary">
                                                        Subscribe
                                                    </button>
                                                </form>
                                            @endif
                                        @endif
                                    </div>

                                    <div class="btn-group">
                                        <a class="btn btn-info" href="/stocks-from-market/{{$stock_data->market_id}}">Volver al listado</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        @if(Session::has('status_message'))
                            <p class="alert alert-success">{{ Session::get('status_message') }}</p>
                        @endif

                        <div class="panel panel-default">
                            <div class="panel-body">
                                {!! $stock_chart !!}
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Value</th>
                                <th>SMA6</th>
                                <th>SMA70</th>
                                <th>SMA200</th>
                                <th>Intersection</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($stock_historicals as $stock)
                                <tr {{ $stock->intersection != '' ? ' class=row-intersection' : ''}}>
                                    <td>{{$stock->date->format('d/m/y')}}</td>
                                    <td>{{$stock->value}}</td>
                                    <td>{{$stock->avg_6}}</td>
                                    <td>{{$stock->avg_70}}</td>
                                    <td>{{$stock->avg_200}}</td>
                                    <td>{!! $stock->intersection !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
