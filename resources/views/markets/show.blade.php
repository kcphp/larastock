@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$title or 'Market'}}</div>
                    <div class="panel-body">
                        @if(Session::has('status_message'))
                            <p class="alert alert-success">{{ Session::get('status_message') }}</p>
                        @endif

                        <p>{{$market->id}} - {{$market->name}} - {{$market->description}} -
                            {{$market->active ? 'activo' : 'inactivo'}}  -
                            <a href="/markets/{{$market->id}}/edit">Editar</a>
                        </p>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href="/markets" class="btn btn-info" role="button">Listado mercados</a>
                                <a href="/markets/create" class="btn btn-info" role="button">Crear mercado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
