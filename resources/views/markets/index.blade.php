@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$title or 'Markets'}}</div>
                    <div class="panel-body">
                        @if(Session::has('status_message'))
                            <p class="alert alert-success">{{ Session::get('status_message') }}</p>
                        @endif

                        @foreach($markets as $market)
                            <div class="row">
                                <div class="col-lg-6 col-xs-6">{{$market->id}} - {{$market->name}} - {{$market->description}} -
                                    {{$market->active ? 'activo' : 'inactivo'}}
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <div class="pull-right">
                                        <a class="btn btn-info" href="/markets/{{$market->id}}">Ver</a>
                                        <a class="btn btn-warning" href="/stocks-from-market/{{$market->id}}">Stocks</a>
                                        <a class="btn btn-success" href="/markets/{{$market->id}}/edit">Editar</a>
                                        <form class="pull-right delete-button" method="POST" action="{{ route('markets.destroy', [$market->id]) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger">Borrar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href="/markets/create" class="btn btn-info" role="button">Crear mercado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
