<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', function() {
    return redirect('markets');
});
Route::get('/home', 'HomeController@index');

Route::get('/markets/create', 'MarketController@create');
Route::post('/markets/create', 'MarketController@store')->name('markets.create');

Route::get('/markets/{id}', 'MarketController@show')->name('markets.show');
Route::get('/markets/{id}/edit', 'MarketController@edit')->name('markets.edit');
Route::put('/markets/{id}/edit', 'MarketController@update')->name('markets.update');
Route::delete('/markets/{id}', 'MarketController@destroy')->name('markets.destroy');

Route::get('/markets/{status?}', 'MarketController@index')->name('markets');

Route::resource('stocks', 'StockController');
Route::get('/stocks-from-market/{market_id}', 'StockController@getStocksFromMarket');

Route::get('/stock_historicals/{stock}/{method}', 'StockHistoricalController@index');
Route::get('/save_stock_historicals/{stock}', 'StockHistoricalController@saveStockHistorical');

Route::get('/stock_historical/{stock_id}', 'StockHistoricalController@getStockHistoricalInfo');
Route::get('/stock_historical_chart/{stock_id}', 'StockHistoricalController@stockHistoricalGraph');


Route::post('/user_stocks', 'UserStocksController@store');
Route::delete('/user_stocks', 'UserStocksController@destroy');

Route::get('/stock_historical/{stock_id}/intersect', function() {
    $stock = \App\StockHistorical::find(63);
    event('App\Events\Intersection', $stock);
});
