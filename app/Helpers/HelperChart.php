<?php

namespace App\Helpers;

use App\Stock;
use App\StockHistorical;
use Carbon\Carbon;
use Khill\Lavacharts\Lavacharts;

class HelperChart
{
    static function generateStockChart($stock_id)
    {
        $lava = new Lavacharts;
        $data = $lava->DataTable();

        $data->addDateColumn('Day of Month')
             ->addNumberColumn('Value')
             ->addNumberColumn('SMA6')
             ->addNumberColumn('SMA70')
             ->addNumberColumn('SMA200');

        // show only last 6 months
        $six_months_ago = Carbon::today()->subMonths(6)->toDateString();

        $stock_values = StockHistorical::getStockHistorical($stock_id, $six_months_ago);
        foreach ($stock_values as $stock_value) {
            $data->addRow([$stock_value->date,
                           $stock_value->value,
                           $stock_value->avg_6,
                           $stock_value->avg_70,
                           $stock_value->avg_200]);
        }


        $lava->LineChart('StockPrices', $data, [
            'titleTextStyle' => [
                'fontName'  => 'Verdana',
                'fontColor' => 'blue',
            ],
            'title'          => 'Gráfico cortes ' . Stock::getStockName($stock_id),
            'legend'         => ['position' => 'bottom'],
        ]);

        $output = '<div id="stocks-chart"></div>';
        $output .= $lava->render('LineChart', 'StockPrices', 'stocks-chart');

        return $output;
    }
}