<?php

namespace App;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $market_id
 * @property string  $name
 * @property string  $acronym
 * @property string  $created_at
 * @property string  $updated_at
 * @property Market  $market
 */
class Stock extends Model
{
    use ValidationTrait;

    protected $fillable = ['market_id', 'name', 'acronym'];
    protected $hidden   = ['created_at', 'updated_at'];

    static function getAllStocksAndMarkets()
    {
        return self::with('market')->get();
    }

    static function getStockId($stock)
    {
        $output = '';
        $stock = self::where('acronym', $stock)->first();
        if ($stock) {
            $output = $stock->getKey();
        }
        return $output;
    }


    static function getAllStocksFromMarket($market_id)
    {
        return self::with(['market' => function ($q) {
            $q->select('id', 'name');
        }])->whereHas('market', function ($q) use ($market_id) {
            $q->where('id', $market_id);
        })->get();
    }


    static function getStockName($stock_id)
    {
        $stock = self::where('id', $stock_id)->first();
        if ($stock) {
            return $stock->name;
        }
        return '';
    }

    static function stockIdExists($stock_id)
    {
        return (bool)self::where('id', $stock_id)->count();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function market()
    {
        return $this->belongsTo('App\Market');
    }
}
