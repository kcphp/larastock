<?php

namespace App\Listeners;

use App\Helpers\HelperIntersection;
use App\User;
use App\Notifications\StockIntersection;
use App\UserStocks;
use Illuminate\Notifications\Notifiable;
use Symfony\Component\Console\Helper\Helper;

class IntersectionListener
{
    use Notifiable;

    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @internal param Intersection $event
     */
    public function handle()
    {
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Intersection',
            'App\Listeners\IntersectionListener@onNewHistoricalValues'
        );
    }

    public function onNewHistoricalValues($stock_values)
    {

        list($icon, $message) = HelperIntersection::checkIntersection($stock_values);

        if (!empty($message)) {
            // look for users following this stock
            $stock_followers = UserStocks::getStockFollowers($stock_values->stock_id);


            foreach ($stock_followers as $follower_id) {
                $user = User::find($follower_id);
                $user->notify(new StockIntersection($stock_values, $message));
            }
        }
    }
}
