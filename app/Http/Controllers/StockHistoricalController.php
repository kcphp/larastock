<?php

namespace App\Http\Controllers;

use App\Helpers\HelperAlphavantage;
use App\Helpers\HelperChart;
use App\Helpers\HelperIntersection;
use App\Stock;
use App\StockHistorical;
use App\UserStocks;
use Illuminate\Support\Facades\Auth;

class StockHistoricalController extends Controller
{

    /**
     * Helper function used to try Alphavantage functions
     *
     * Documentation:
     * http://www.alphavantage.co/documentation/#daily
     *
     * To be called like this /stock_historicals/ACS.MC/TIME_SERIES_DAILY
     */
    public function index($stock, $method)
    {
        $params = ['function' => $method, 'symbol' => $stock, 'outputsize' => 'compact'];
        echo HelperAlphavantage::getJsonReply($params);
    }


    /**
     * Here is where we retrieve the stock historical data and save it in database
     *
     * https://www.alphavantage.co/query?function=SMA&symbol=MSFT&interval=15min&time_period=10&series_type=close&apikey=demo
     * @param $stock
     *
     * http://larastock.devel/save_stock_historicals/ACS.MC
     */
    public function saveStockHistorical($stock)
    {

        $stock_values_processed = $this->_getStockClosingValues($stock);

        $sma_6_processed = $this->_getStockSMA($stock, 6);
        $sma_70_processed = $this->_getStockSMA($stock, 70);
        $sma_200_processed = $this->_getStockSMA($stock, 200);

        $stock_id = Stock::getStockId($stock);

        if (is_array($stock_values_processed)) {
            $stock_historical = new StockHistorical();

            foreach ($stock_values_processed as $date => $stock_value) {
                $input = ['stock_id' => $stock_id,
                          'date'     => $date,
                          'value'    => $stock_value,
                          'avg_6'    => $sma_6_processed[$date],
                          'avg_70'   => $sma_70_processed[$date],
                          'avg_200'  => $sma_200_processed[$date],];

                if ($stock_historical->validate($input)) {
                    $stock_historical_saved = StockHistorical::create($input);
                    echo "\nSaved values of $stock from date:  $date\n";

                    // only trigger the event for today values
                    // (in case we had the app stopped and when we run it,
                    // several days have gone
                    if (HelperAlphavantage::isStockHistoricalFromToday($date)) {
                        event('App\Events\Intersection', $stock_historical_saved);
                    }
                } else {
                    //\Debugbar::warning($stock_historical->errors);
                    //echo "$stock_historical->errors\n";
                }
            }
        }
    }


    public function getStockHistoricalInfo($stock_id)
    {
        $stock_name = Stock::getStockName($stock_id);
        $stock_data = Stock::find($stock_id);
        $stock_historical = StockHistorical::getStockHistorical($stock_id);

        HelperIntersection::getIntersections($stock_historical);

        $user_stocks = [];
        if (Auth::check()) {
            $user_stocks = array_keys(UserStocks::getUserStocks(Auth::id()));
        }

        return view('stock_historicals.index')
            ->withTitle('Historical ' . $stock_name)
            ->with(['stock_historicals' => $stock_historical,
                    'stock_data'        => $stock_data,
                    'user_stocks'       => $user_stocks,
                    'stock_chart'       => HelperChart::generateStockChart($stock_id)]);
    }


    public function stockHistoricalChart($stock_id)
    {
        echo HelperChart::generateStockChart($stock_id);
    }


    /**
     * @param $stock
     * @return array
     */
    private function _getStockClosingValues($stock)
    {
        $params = ['function'   => config('larastock.closing_values_method'),
                   'symbol'     => $stock,
                   'outputsize' => 'compact'];
        $stock_values = HelperAlphavantage::getArrayReply($params);

        return HelperAlphavantage::processArray($stock_values, true);
    }

    /**
     * @param $stock
     * @param $sma_period
     * @return array
     */
    private function _getStockSMA($stock, $sma_period)
    {
        $params = ['function'    => config('larastock.moving_average_values_method'),
                   'symbol'      => $stock,
                   'interval'    => 'daily',
                   'time_period' => $sma_period,
                   'series_type' => 'close',
                   'outputsize'  => 'compact'];

        $sma = HelperAlphavantage::getArrayReply($params);

        return HelperAlphavantage::processArray($sma);
    }

}
