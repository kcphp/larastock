<?php

namespace App\Http\Controllers;

use App\Stock;
use App\UserStocks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stocks = Stock::getAllStocksAndMarkets();

        echo 'valor: ' . $stocks[0]->name . '<br />';
        echo 'acronimo: ' . $stocks[0]->acronym . '<br />';
        echo 'mercado: ' . $stocks[0]->market->name . '<br />';
        echo 'acronimo mercado: ' . $stocks[0]->market->acronym;
    }


    public function getStocksFromMarket($market_id)
    {
        $stocks = Stock::getAllStocksFromMarket($market_id);

        $user_stocks = [];
        if (Auth::check()) {
            $user_stocks = array_keys(UserStocks::getUserStocks(Auth::id()));
        }

        return view('stocks.index')->with(['stocks' => $stocks, 'user_stocks' => $user_stocks]);
    }

}
