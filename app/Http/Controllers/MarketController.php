<?php

namespace App\Http\Controllers;

use App\Market;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MarketController extends Controller
{
    /**
     * @param string $status
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($status = 'all')
    {
        if ($status == 'active') {
            $markets = Market::getActiveMarkets();
        } else {
            $markets = Market::getAllMarkets();
        }

        $data = ['title'   => 'Listado de mercados',
                 'markets' => $markets];

        return view('markets.index', $data);
    }

    public function create()
    {
        return view('markets.create');
    }

    public function store(Request $request)
    {

        $market = new Market();

        $input = $request->all();

        if ($market->validate($input)) {
            // solves default active value set to true
            $input['active'] = $request->active ?: 0;
            Market::create($input);

            Session::flash('status_message', 'Market added!');

            return redirect('markets');
        } else {
            return back()->withInput($input)->withErrors($market->errors);
        }
    }

    public function show($id)
    {

        $market = Market::findOrFail($id);

        return view('markets.show')->withTitle('Detalle de mercado')->with('market', $market);
    }

    public function edit($id)
    {

        $market = Market::findOrFail($id);

        return view('markets.edit')->withTitle('Detalle de mercado')->with('market', $market);
    }

    public function update(Request $request, $id)
    {

        $market = Market::findOrFail($id);
        $input = $request->all();

        if ($market->validate($input)) {

            $market->name = $request->name;
            $market->description = $request->description;
            $market->active = (bool)$request->active;
            $market->save();

            Session::flash('status_message', 'Market edited!');

            return redirect('markets');
        } else {
            return back()->withInput($input)->withErrors($market->errors);
        }
    }

    public function destroy($id)
    {
        try {
            $market = Market::findOrFail($id);
            $market->delete();

            $status_message = 'Market deleted!';

        }
        catch (ModelNotFoundException $e) {
            $status_message = 'no market with that ID';
        }

        Session::flash('status_message', $status_message);

        return redirect('markets');
    }

}
