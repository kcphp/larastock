<?php

namespace App\Http\Controllers;

use App\UserStocks;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_stocks = UserStocks::getUserStocksData(Auth::id());
        return view('home', ['user_stocks' => $user_stocks]);
    }
}
