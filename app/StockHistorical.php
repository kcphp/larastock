<?php

namespace App;

use App\Traits\ValidationTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $stock_id
 * @property float   $value
 * @property float   $avg_6
 * @property float   $avg_70
 * @property float   $avg_200
 * @property string  $created_at
 * @property string  $updated_at
 * @property Stock   $stock
 */
class StockHistorical extends Model
{

    use ValidationTrait;

    protected $fillable = ['stock_id', 'date', 'value', 'avg_6', 'avg_70', 'avg_200'];
    protected $hidden   = ['created_at', 'updated_at'];
    protected $dates    = ['date'];

    protected $rules = [
        'date'     => 'required|date_format:Y-m-d',
        'stock_id' => 'required|integer',
        'value'    => 'required|numeric|unique_with:stock_historicals,date,value',
        'avg_6'    => 'required|numeric',
        'avg_70'   => 'required|numeric',
        'avg_200'  => 'required|numeric',
    ];

    /**
     * @param      $stock_id
     * @param null $start_date
     * @param null $end_date
     * @return mixed
     */
    static function getStockHistorical($stock_id, $start_date = null, $end_date = null)
    {
        $query = StockHistorical::where('stock_id', $stock_id);
        if (!is_null($start_date)) {
            $query->where('date', '>=', $start_date);
        }
        if (!is_null($end_date)) {
            $query->where('date', '<=', $end_date);
        }

        return $query->orderBy('date', 'DESC')->get();
    }

    /**
     * @param $stock_id
     * @return bool
     */
    static function stockHasAlreadyBeenSaved($stock_id)
    {
        return (bool)StockHistorical::where('stock_id', $stock_id)->count();
    }

    /**
     * @param $stock_id
     * @return bool
     */
    static function stockHasAlreadyBeenSavedToday($stock_id)
    {
        return (bool)StockHistorical::where('stock_id', $stock_id)
                                    ->whereDate('created_at', Carbon::now()->toDateString())
                                    ->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stock()
    {
        return $this->belongsTo('App\Stock');
    }
}
