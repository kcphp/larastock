# Larastock

This is a demo project used in Keepcoding's PHP course.


![Larastock](https://bytebucket.org/kcphp/larastock/raw/61eae2b971198b6c0d48e720f34239b5606f78b9/public/img/larastock.png)



## APIs and Libraries used

The API used to get the stock values is 

* [Krlove model generator](https://github.com/krlove/eloquent-model-generator)
* [Kevinkhill Lavacharts](https://github.com/kevinkhill/lavacharts) - [Official site](http://lavacharts.com/)
* [Felixkiss UniqueWithValidator](https://github.com/felixkiss/uniquewith-validator)
* [Barryvdh Debugbar](https://github.com/barryvdh/laravel-debugbar)

Mails are sent using [Mailgun](mailgun.com)


## Config

* Clone this project 
* Duplicate .env.example file and rename it to .env
* Set database config and App Name
* Run composer install
* Run npm install and after that npm run dev (or npm run watch)
* Run php artisan key:generate
* Run php artisan serve
* Run php artisan migrate
* Seed the database: php artisan db:seed --class=IbexSeeder
* Get a free API key from Alphavantage in https://www.alphavantage.co/support/#api-key and set it in your .env file
* Get an API key from Mailgun and set it in the .env file in MAILGUN_SECRET (also you can set your own smtp service)
* Configure cron in your environment (change your absolute path)

`crontab -e ` 

`* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1`


* Run manually the first attempt to get values:

`php artisan run stock_historicals:get_data`